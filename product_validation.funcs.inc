<?php
/**
 * @file universal functions.
 */

/**
 * @param $type  content type
 * return value: array of nids.
 */
function get_nid_by_type($type) {
  $nids = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', $type)
    ->execute()
    ->fetchCol();
  return $nids;
}

/**
 * generate verifition code.
 */
function generate_code ($nid, $title) {
  $nid = db_insert('product_validation')
    ->fields(array(
      'nid' => $nid,
      'uuid' => create_uuid(),
      'title' => $title,
    ))
    ->execute();
}

/**
 * create uuid.
 */
function create_uuid()  {
  $uuid = '';
  if (sizeof(module_implements('generate_uuid')) > 0) {
    $uuid = module_invoke_all('generate_uuid');
  } else {
    $uuid = uniqid();
  }
  $result = db_select('product_validation', 'a')
    ->condition('a.uuid', $uuid)
    ->fields('a', array('uuid'))
    ->execute();
  $num = $result->rowCount();
  if ($num) {
    create_uuid();
  }
  else {
    return  $uuid;
  }
}