Product Validation
--------------------------

Install
-------
* Enable the module
* Go to Administer > Settings > Product validation to change settings
* Go to Administration > Structure to set where to shows for the product validation block


Description
-----------
This module allows you set a content type to generate the verification code for each node.
Generally,the code use for like  "product anti-fake".

The module adds options to select:

- which content type node to generate the verification code  ,
- what message to display when user use the module interface to verify the authenticity of their product.

On the product validation page, you can:

- view the verification code list,
- delete them by the 'delete' or 'clera' link ,
- generate verification code for each node in the seleted content type by the 'generate' link,


