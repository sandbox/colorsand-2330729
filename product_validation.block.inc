<?php
/**
 * Implements hook_block_info().
 */
function product_validation_block_info() {
  $block['product_validation'] = array(
    'info' => 'product validation',
  );
  return $block;
}

/**
 * Implements hook_block_view().
 */
function product_validation_block_view($delta = '') {
  switch ($delta) {
    case 'product_validation':
      $block['subject'] = t('product validation');
      $block['content'] = drupal_get_form('product_validation_form');
      return $block;
      break;
  }
}

function product_validation_form($form, &$form_state) {
  $form['antiCounterFeitiing'] = array(
    '#title' => t(''),
    '#type' => 'textfield',
    '#maxlength' => 20,
    '#size' => 20,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('verification'),
  );
  return $form;

}
function product_validation_form_validate($form, &$form_state)  {
}
function product_validation_form_submit($form, &$form_state) {
  $num = '';
  if ($uuid = $form_state['values']['antiCounterFeitiing']) {
    $query = db_select('product_validation', 'a')
      ->condition('a.uuid', $uuid)
      ->fields('a', array('uuid'))
      ->execute();
    $num = $query->rowCount();
  }
  if ($num) {
    drupal_set_message(variable_get('correct'),'valid');
  } else {
    drupal_set_message(variable_get('incorrect'),'invalid');
  }
}
