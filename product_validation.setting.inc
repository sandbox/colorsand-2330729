<?php
function product_validation_admin_form()  {
  $form = array();
  $form['validation_content_type'] = array(
    '#type' => 'select',
    '#title' => t('centent types'),
    '#description' => t('select the content type to generate Validation code'),
    '#options' => node_type_list(),
  );
  $form['product_validation_message'] = array(
    '#type' => 'fieldset',
    '#title' => t('message'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['product_validation_message']['correct'] = array(
   '#type' => 'textfield',
   '#title' => 'message for the valid verification',
   '#size' => 60,
   '#maxleng' => 60,
   '#default_value' => t('Validation code you entered is valid!'),
  );
  $form['product_validation_message']['incorrect'] = array(
   '#type' => 'textfield',
   '#title' => 'message for the invalid verification',
   '#size' => 60,
   '#maxleng' => 60,
   '#default_value' => t('Validation code you entered is invalid!'),
    );
  return system_settings_form($form);
}
function product_validation_admin_form_validate($form, &$form_state)  {
  //TODO
}
function node_type_list()  {
  $list = array();
  $options = array();
  $list = node_type_get_types();
  foreach ($list as $k=>$v) {
    $options[$k] = $v->name;
  }
  return $options;
}

function Validation_code_list($form, &$form_state)  {
  $header = array(
    array('data' => 'NODE TITLE', 'field' => 'title', 'sort' => 'asc'),
    array('data' => 'NODE ID', 'field' => 'nid'),
    array('data' => 'VERIFICATION CODE', 'field' => 'uuid'),
    array('data' => 'OPERATIONS')
  );
  $query = db_select('product_validation', 'a')
    ->extend('PagerDefault')
      ->limit(10)
    ->extend('TableSort')
      ->orderByHeader($header);
  $query->fields('a', array('nid', 'uuid', 'title'));
  $result = $query->execute();
  $rows = array();
  foreach ($result as $v) {
    $rows[] = array($v->title, $v->nid, $v->uuid, l(t('delete'), 'admin/product_code/'.$v->nid.'/del'));
  }
 $form['content'] = array(
    '#theme' =>'table',
    '#caption' => '<h2>' .'Verification Code List' .'</h2>',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => 'There is no data',
    '#sticky' => true,
  );

  $form['pager'] = array(
    '#theme' => 'pager',
    '#weight' => 5,
  );

  $form['clear'] = array(
    '#type' => 'link',
    '#title' => 'clear',
    '#href' => 'admin/product_code/clear',
    '#submit' => 'product_code_clear_submit',
  );
  $form['generate'] = array(
    '#type' => 'link',
    '#title' => 'generate',
    '#href' => 'admin/product_code/generate',
    '#prefix' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',

  );

  return $form;
}

function Validation_code_list_submit($form, &$form_state)  {
  dpm($form_state);
}

function product_code_clear($form, &$form_state) {
  return confirm_form(array(
    'feed' => array(
      '#type' => 'value',
      '#value' => 'hello',
    ),
  ), t('Are you sure you want to remove all data from the verfition code?'
    ), 'admin/code_list',
    t('This action cannot be undone.'), t('Remove code'), t('Cancel'));
}

function product_code_clear_submit () {
  db_truncate('product_validation')->execute();
  drupal_goto('admin/code_list');
}

/**
 * generate verification code for every node of the assigned content type.
 */
function product_code_generate() {
  if(!$type = variable_get('validation_content_type'))
  {
    return drupal_set_message(t('Please first at set the content type ') .l(t('setting'), 'admin/confing/product_validation'));
  }
  $nids = get_nid_by_type($type);

  $v_nids = db_select('product_validation', 'p')
    ->fields('p', array('nid'))
    ->execute()
    ->fetchCol();
  if($v_nids) {
    $nids = array_diff($nids, $v_nids);
  }

  if(!$nids) {
    return drupal_set_message('nothing for generate');
  }
  foreach($nids as $nid) {
    $title = node_load($nid)->title;
    generate_code($nid, $title);
  }
  return drupal_goto('admin/code_list');
}

function product_code_del($nid) {
  db_delete('product_validation')
    ->condition('nid', $nid)
    ->execute();
  drupal_goto('admin/code_list');
}